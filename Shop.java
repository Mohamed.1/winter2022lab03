import java.util.*;

public class Shop
{
	public static void main(String []args)
	{
		Fruit[] fruits = new Fruit[4];
		for(int i = 0; i < fruits.length ; i++)
		{
			Scanner scan = new Scanner(System.in);
			fruits[i] = new Fruit();
			System.out.println("Part "+(1+i));
			System.out.println("Enter the type of fruit (no cap)");
			fruits[i].type = scan.nextLine();
			System.out.println("Enter the color of the fruit");
			fruits[i].color = scan.nextLine();
			System.out.println("Enter the height of the fruit");
			fruits[i].height = Integer.parseInt(scan.nextLine());
		}
		System.out.println("Type of last fruit: "+fruits[3].type);
		System.out.println("Color of last fruit: "+fruits[3].color);
		System.out.println("Height of last fruit: "+fruits[3].height);
		
		fruits[3].getHeight();
	}
}